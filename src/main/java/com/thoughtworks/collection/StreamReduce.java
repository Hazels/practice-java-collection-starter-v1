package com.thoughtworks.collection;

import java.util.List;

import static java.util.Collections.max;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream().reduce((odd,number) -> number).orElse(-1);
    }

    public String getLongest(List<String> words) {
        return words.stream().reduce((identity,word) -> word.length() > identity.length() ? word:identity).orElse("");
    }

    public int getTotalLength(List<String> words) {
        return words.stream().reduce(0,(initValue,word)->initValue + word.length(),Integer::max);
    }
}
